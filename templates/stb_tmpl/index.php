<?php
/**
 * @package                Joomla.Site
 * @subpackage        Templates.beez5
 * @copyright        Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license                GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

//include template Functions CG
include_once(JPATH_ROOT . "/templates/" . $this->template . '/template_functions.php');

$doc = JFactory::getDocument();

/*
 * Bootstrap include
 * Add JavaScript Frameworks
 */
//$bootstrap      = explode(',', $templateparams->get('bootstrap'));
JHtml::_('bootstrap.framework');
JHtml::_('bootstrap.loadCss');
// $doc->addScript($this->baseurl . '/templates/' . $this->template . '/javascript/md_stylechanger.js', 'text/javascript');
// $doc->addScript($this->baseurl . '/templates/' . $this->template . '/javascript/hide.js', 'text/javascript');
// $doc->addScript($this->baseurl . '/templates/' . $this->template . '/javascript/respond.src.js', 'text/javascript');
// $doc->addScript($this->baseurl . '/templates/' . $this->template . '/javascript/template.js', 'text/javascript');


// check modules
$showRightColumn        = ($this->countModules('position-3') or $this->countModules('position-6') or $this->countModules('position-8'));
$showbottom                        = ($this->countModules('position-9') or $this->countModules('position-10') or $this->countModules('position-11'));
$showleft                        = ($this->countModules('position-4') or $this->countModules('position-7') or $this->countModules('position-5'));

if ($showRightColumn==0 and $showleft==0) {
        $showno = 0;
}

JHtml::_('behavior.framework', true);

// get params
$color                        = $this->params->get('templatecolor');
$logo                        = $this->params->get('logo');
$navposition        = $this->params->get('navposition');
$app                        = JFactory::getApplication();
$templateparams        = $app->getTemplate(true)->params;


//ad scripts CG
$doc->addScript($this->baseurl.'/templates/'.$this->template.'/javascript/md_stylechanger.js', 'text/javascript', true);
?>
<?php if(!$templateparams->get('html5', 0)): ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php else: ?>

<?php endif; ?>
<html lang="de-De">
        <head>

                <jdoc:include type="head" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0">                
                <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/system.css" type="text/css" />
                <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/position.css" type="text/css" media="screen,projection" />
                <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/layout.css" type="text/css" media="screen,projection" />
                <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/print.css" type="text/css" media="Print" />
                <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/beez5.css" type="text/css" />
                <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/overrides.css" type="text/css" />
                <link rel='stylesheet' href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<?php
        $files = JHtml::_('stylesheet', 'templates/'.$this->template.'/css/general.css', null, false, true);
        if ($files):
                if (!is_array($files)):
                        $files = array($files);
                endif;
                foreach($files as $file):
?>
                    <link rel="stylesheet" href="<?php echo $file;?>" type="text/css" />
<?php
                 endforeach;
        endif;
?>
                <?php if ($this->direction == 'rtl') : ?>
                <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/template_rtl.css" type="text/css" />
                <?php endif; ?>
                <!--[if lte IE 6]>
                        <link href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/ieonly.css" rel="stylesheet" type="text/css" />
                <![endif]-->
                <!--[if IE 7]>
                        <link href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/ie7only.css" rel="stylesheet" type="text/css" />
                <![endif]-->
                 <!--[if IE 8]>
                        <link href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/ie8only.css" rel="stylesheet" type="text/css" />
                <![endif]-->
<?php if($templateparams->get('html5', 0)) { ?>
                <!--[if lt IE 9]>
                        <script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/javascript/html5.js"></script>
                <![endif]-->
<?php } ?>
                <? /*<script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/javascript/hide.js"></script> */?>



        </head>

        <body id="body" class="site">
<div id="all" class="container-fluid">
        <div id="back" class="wrapper">
        <?php if(!$templateparams->get('html5', 0)): ?>
                <div id="header">
                        <?php else: ?>
                <header id="header" class="row-fluid">
                        <?php endif; ?>
                                <div class="logoheader span12">
                                        <h1 id="logo">
                                             <a href="http://www.steuerberaterin-gilching.de" title="Steuerberatung Gilching - Steuerberaterin Heidi Harasser">
                                                <!-- <img src="/images/hppix/stb-harasser-head.png" alt="Steuerberatung Gilching - Steuerberaterin Heidi Harasser" /> -->
                                             
                                                <?php if ($logo != null ): ?>
                                                    <img src="<?php echo $this->baseurl ?>/<?php echo htmlspecialchars($logo); ?>" alt="<?php echo htmlspecialchars($templateparams->get('sitetitle'));?>" />
                                                <?php else: ?>
                                                    <?php echo htmlspecialchars($templateparams->get('sitetitle'));?>
                                                <?php endif; ?>
                                                    <?php if(!empty($templateparams->get('sitedescription'))) { ?>
                                                        <span class="header1">
                                                            <?php echo htmlspecialchars($templateparams->get('sitedescription'));?>
                                                        </span>
                                                    <?php } ?>
                                            </a>
                                        </h1>
                                </div><!-- end logoheader -->

                                        <ul class="skiplinks">
                                                <li><a href="#main" class="u2"><?php echo JText::_('TPL_BEEZ5_SKIP_TO_CONTENT'); ?></a></li>
                                                <li><a href="#nav" class="u2"><?php echo JText::_('TPL_BEEZ5_JUMP_TO_NAV'); ?></a></li>

                                        </ul>
                                        <h2 class="unseen"><?php echo JText::_('TPL_BEEZ5_NAV_VIEW_SEARCH'); ?></h2>
                                        <h3 class="unseen"><?php echo JText::_('TPL_BEEZ5_NAVIGATION'); ?></h3>
                                        <jdoc:include type="modules" name="position-1" />
                                        <div id="line">
                                        <div id="fontsize"></div>
                                        <h3 class="unseen"><?php echo JText::_('TPL_BEEZ5_SEARCH'); ?></h3>
                                        <jdoc:include type="modules" name="position-0" />
                                        </div> <!-- end line -->
                <div id="header-image">
                        <jdoc:include type="modules" name="position-15" />
                        <?php if ($this->countModules('position-15')==0): ?>

                        <?php endif; ?>
                </div>
                <?php if (!$templateparams->get('html5', 0)): ?>
                        </div><!-- end header -->
                <?php else: ?>
                        </header><!-- end header -->
                <?php endif; ?>
                <div id="<?php echo $showRightColumn ? 'contentarea2' : 'contentarea'; ?>" class="row-fluid">
                                        <div id="breadcrumbs">
                                            <div class="span12 navbar">
                                                    <div class="mobile-nav-menu<? //xxx_navbar-inner ?>">
                                                        <span class="nav_trigger">
                                                            <i class="fa fa-navicon"></i>
                                                        </span>                                                                            
                                                            <div class="nav-collapse collapse "  role="navigation">
                                                                <jdoc:include type="modules" name="position-2"/>
                                                            </div>                                                                  
                                                    <!--/.nav-collapse -->
                                                    </div><!-- /.navbar-inner -->
                                            </div><!-- /.navbar -->
                                        </div>
                <div class="clr"></div>

                                        <?php if ($navposition=='left' and $showleft) : ?>

                                                <?php if(!$this->params->get('html5', 0)): ?>
                                                        <div class="left1 <?php if ($showRightColumn==NULL){ echo 'leftbigger';} ?>" id="nav">
                                                <?php else: ?>
                                                        <nav class="left1 <?php if ($showRightColumn==NULL){ echo 'leftbigger';} ?>" id="nav">
                                                <?php endif; ?>

                                                                <jdoc:include type="modules" name="position-7" style="beezDivision" headerLevel="3" />
                                                                <jdoc:include type="modules" name="position-4" style="beezHide" headerLevel="3" state="0 " />
                                                                <jdoc:include type="modules" name="position-5" style="beezTabs" headerLevel="2"  id="3" />

                                                <?php if(!$this->params->get('html5', 0)): ?>
                                                        </div><!-- end navi -->
                                                <?php else: ?>
                                                        </nav>
                                                <?php endif; ?>

                                        <?php endif; ?>

                                        <div id="<?php echo $showRightColumn ? 'wrapper' : 'wrapper2'; ?>" class="row-fluid span6"<?php if (isset($showno)){echo 'class="shownocolumns"';}?>>

                                                <div id="main">

                                                <?php if ($this->countModules('position-12')): ?>
                                                        <div id="top"><jdoc:include type="modules" name="position-12"   />
                                                        </div>
                                                <?php endif; ?>

                                                        <jdoc:include type="message" />
                                                        <jdoc:include type="component" />

                                                </div><!-- end main -->

                                        </div><!-- end wrapper -->

                                <?php if ($showRightColumn) : ?>
                                        <h2 class="unseen">
                                                <?php echo JText::_('TPL_BEEZ5_ADDITIONAL_INFORMATION'); ?>
                                        </h2>


                                <?php if (!$templateparams->get('html5', 0)): ?>
                                        <div id="right">
                                <?php else: ?>
                                        <aside id="right" class="span6 offset1">
                                <?php endif; ?>

                                                <a id="additional"></a>
                                                <jdoc:include type="modules" name="position-6" style="beezDivision" headerLevel="3"/>
                                                <jdoc:include type="modules" name="position-8" style="beezDivision" headerLevel="3"  />
                                                <jdoc:include type="modules" name="position-3" style="beezDivision" headerLevel="3"  />

                                <?php if(!$templateparams->get('html5', 0)): ?>
                                        </div><!-- end right -->
                                <?php else: ?>
                                        </aside>
                                <?php endif; ?>
                        <?php endif; ?>

                        <?php if ($navposition=='center' and $showleft) : ?>

                                <?php if (!$this->params->get('html5', 0)): ?>
                                        <div class="left <?php if ($showRightColumn==NULL){ echo 'leftbigger';} ?>" id="nav" >
                                <?php else: ?>
                                        <nav class="left <?php if ($showRightColumn==NULL){ echo 'leftbigger';} ?>" id="nav">
                                <?php endif; ?>

                                                <jdoc:include type="modules" name="position-7"  style="beezDivision" headerLevel="3" />
                                                <jdoc:include type="modules" name="position-4" style="beezHide" headerLevel="3" state="0 " />
                                                <jdoc:include type="modules" name="position-5" style="beezTabs" headerLevel="2"  id="3" />

                                <?php if (!$templateparams->get('html5', 0)): ?>
                                        </div><!-- end navi -->
                                <?php else: ?>
                                        </nav>
                                <?php endif; ?>
                        <?php endif; ?>

                                        <div class="wrap"></div>

                                </div> <!-- end contentarea -->

                        </div><!-- back -->

                </div><!-- all -->

                <div id="footer-outer">

                <?php if ($showbottom) : ?>
                        <div id="footer-inner">

                                <div id="bottom">
                                        <?php if ($this->countModules('position-9')): ?>
                                        <div class="box box1"> <jdoc:include type="modules" name="position-9" style="beezDivision" headerlevel="3" /></div>
                                        <?php endif; ?>
                                           <?php if ($this->countModules('position-10')): ?>
                                        <div class="box box2"> <jdoc:include type="modules" name="position-10" style="beezDivision" headerlevel="3" /></div>
                                        <?php endif; ?>
                                        <?php if ($this->countModules('position-11')): ?>
                                        <div class="box box3"> <jdoc:include type="modules" name="position-11" style="beezDivision" headerlevel="3" /></div>
                                        <?php endif ; ?>
                                </div>
                        </div>
                <?php endif ; ?>

                        <div id="footer-sub">

                        <?php if (!$templateparams->get('html5', 0)): ?>
                                <div id="footer">
                        <?php else: ?>
                                <footer id="footer">
                        <?php endif; ?>

                                        <jdoc:include type="modules" name="position-14" />
                                        <p>
                                          &copy; <?php echo date("Y");?> Steuerberaterin Heidi Harasser - Gilching
                                        </p>

                        <?php if (!$templateparams->get('html5', 0)): ?>
                                </div><!-- end footer -->
                        <?php else: ?>
                                </footer>
                        <?php endif; ?>   <div id="footer3"> <jdoc:include type="modules" name="position-30" />     </div>

                                             <jdoc:include type="modules" name="footer" />

                        </div>

                </div>

                <jdoc:include type="modules" name="debug" />

<?// toggle Menue script siehe: http://www.jqueryscript.net/menu/Responsive-Sidebar-Push-Menu-with-jQuery-CSS3.html?>
<script type="text/javascript">

    jQuery(".nav_trigger").click(function() {
        $("body").toggleClass("show_sidebar");
        $(".nav_trigger .fa").toggleClass("fa-navicon fa-times"); 
        });
    /*Bei ausgeklappten Menue und resize über 767px body-klasse wieder entfernen*/
    jQuery(window).on('resize', function(){
          var win = $(this); //this = window
    if (win.width() > 768 ) {
        $("body").removeClass("show_sidebar");
        $(".nav_trigger .fa").addClass("fa-navicon");
    }
    });

</script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>                
        </body>
</html>
